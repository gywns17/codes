// 패키지 선언부
package stream;



// 클래스 선언부
public class JavaSample {
    //
    // =====================   필드 선언부
    // [modifier] [type] [fieldName];
    private String field1;
    // [modifier] [static] [type] [fieldName];
    // static -> 클래스에서 접근 가능
    private static String field2;
    // final -> 반드시 초기화 되어야 함.
    private static final String field3 = "must initiated";
    // final -> 반드시 초기화 되어야 함.|| 생성자를 통한 초기화도 가능
    private final String field4 ;


    // =====================   생성자
    // [modifier] [ClassName] ([Parameters])
    public JavaSample(String field4){
        this.field4 = field4;
        // final로 선언된 필드는 한번 초기화된 후에는 수정 불가
//        this.field3 = "modify";
    }

    public JavaSample(String field1, String field2, String field4){
        //
        // this() --> 자신의 생성자를 호출...
        this(field4);
        this.field1 = field1;
        this.field2 = field2;
    }

    // =====================   메서드 선언부
    // [modifier] [return type] [methodName](([Parameters])
    public void introduceMySelf(){
        //
        // this는 인스턴스(또는 객체) 자기 자신을 가리키는 변수
        System.out.println(this.toString());
    }
    // static 메서드는 클래스의 메서드
    public static void introduceMyClazz(){
        //
        // static 메소드는 클래스의 메서드로 인스턴스가 만들어 지지 않아
//         this 키워드 사용 불가
//        this.field1;

        System.out.println(JavaSample.class.getName());
    }

    // Java가 실행되기 위해선 무조건 main 메소드가 선언되어야 함.
    // 실행된다는 것은 Java가 구동되는 JVM(Java virtual Machine에
    // 프로세스(스레드)가 생성되어 실행된다는 의미
    public static void main(String[] args) {
        // static 선언된 것은 클래스에서 접근
        JavaSample.introduceMyClazz();
        String staticField2 = JavaSample.field2;
        String staticField3 = JavaSample.field3;
//        Sample.field1;

        // static이 아니면 인스턴스(또는 객체)를 통해 접근 가능
        JavaSample javaSampleInstance = new JavaSample("hello");
        String instanceField1 = javaSampleInstance.field1;
        String instanceField4 = javaSampleInstance.field4;
        javaSampleInstance.introduceMySelf();
    }
}
