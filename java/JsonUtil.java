package jdbc.etc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JsonUtil {
    //
    public static final Gson gson = new Gson();
    public static final Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();

    public static String toJson(Object object){
        //
        return gson.toJson(object);
    }

    public static <T> T fromJson(String json, Class<T> type){
        //
        return gson.fromJson(json, type);
    }

    public static <T> List<T> fromJsonList(String json, Class<T> type){
        //
        List<T> objects = gson.fromJson(json, new TypeToken<List<T>>(){}.getType());
        List<T> results = objects.stream()
                            .map(treeMap -> {
                                String treeMapJson = gson.toJson(treeMap);
                                return fromJson(treeMapJson, type);
                            })
                            .collect(Collectors.toList());
        return results;
    }

    @AllArgsConstructor
    @Getter
    @Setter
    static class Sample {
        private String name;
        private int age;
    }

    public static void main(String[] args) {
        // toJson
        System.out.println("======== toJson Test ============");
        Sample sample = new Sample("ksyoo", 30);
        String sampleJson = JsonUtil.toJson(sample);
        System.out.println(sampleJson);

        // fromJson
        System.out.println("======== fromJson Test ============");
        Sample fromJsonSample = JsonUtil.fromJson(sampleJson, Sample.class);
        System.out.println(JsonUtil.toJson(fromJsonSample));

        // fromJsonList
        System.out.println("======== fromJsonList Test ============");
        List<Sample> samples = Arrays.asList(
                new Sample("first", 1),
                new Sample("second", 2),
                new Sample("third", 3));
        String samplesJson = JsonUtil.toJson(samples);
        List<Sample> fromJsonSamples = JsonUtil.fromJsonList(samplesJson, Sample.class);
        fromJsonSamples.forEach(s -> System.out.println(s.getName()));
    }
}
